from django.contrib import admin
from restapi.app.models import *

admin.site.register(Course)
admin.site.register(Module)
admin.site.register(Task)