from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from .models import *
from .serializers import *

class CourseViewSet(mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):
    pagination_class = PageNumberPagination
    queryset = Course.objects.all().order_by("created_data")
    serializer_class = CourseSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        queryset = self.queryset

        if "name" in self.request.query_params:
            queryset = queryset.filter(name__icontains=self.request.query_params.get("name"))

        return queryset

    def get_serializer_class(self):
        if self.action == "list":
            return CourseSerializer
        else:
            return ThisCourseSerializer