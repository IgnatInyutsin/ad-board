from restapi.app.models import *
from rest_framework import serializers

# Вложенный сериализатор для отображения конкретного курса
class ShortModuleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Module
        fields = ["id", "name", "author"]

# Сериализатор для всех курсов
class CourseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Course
        fields = ["id", "name", "author", "created_data"]

# Сериализатор для отдельного курса
class ThisCourseSerializer(serializers.HyperlinkedModelSerializer):
    modules = ShortModuleSerializer(many=True)
    class Meta:
        model = Course
        fields = ["id", "name", "author", "created_data", "modules"]