from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from restapi.app.models import *
from .serializers import *

class ModuleViewSet(mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):
    queryset = Module.objects.all().order_by("created_data")
    serializer_class = ModuleSerializer
    permission_classes = (AllowAny,)