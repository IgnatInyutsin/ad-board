from restapi.app.models import *
from rest_framework import serializers

# Сериализатор для задания
class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ["id", "title", "text", "created_data", "answer"]

# Сериализатор для модуля
class ModuleSerializer(serializers.HyperlinkedModelSerializer):
    tasks = TaskSerializer(many=True)

    class Meta:
        model = Module
        fields = ["id", "name", "author", "created_data", "tasks"]