# Generated by Django 4.0.4 on 2022-09-13 13:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.TextField()),
                ('created_data', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Module',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.TextField()),
                ('created_data', models.DateTimeField(auto_now_add=True)),
                ('course', models.ManyToManyField(to='app.course')),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('title', models.TextField()),
                ('text', models.TextField()),
                ('created_data', models.DateTimeField(auto_now_add=True)),
                ('answer', models.IntegerField()),
                ('module', models.ManyToManyField(to='app.module')),
            ],
        ),
    ]
