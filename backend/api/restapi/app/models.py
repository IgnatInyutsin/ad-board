import uuid
from django.db import models
from multiselectfield import MultiSelectField

'''
Модель курса.
На нее ссылаются модели компонентов
'''

class Course(models.Model):
    id = models.AutoField(primary_key=True)
    # Имя курса
    name = models.TextField()
    # Имя автора/авторов
    author = models.TextField()
    # Дата создания
    created_data = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

'''
Модель модуля.
Ссылается на курс, на нее ссылаются задания
'''

class Module(models.Model):
    id = models.AutoField(primary_key=True)
    # Имя модуля
    name = models.TextField()
    # Имя автора/авторов
    author = models.TextField()
    # Дата создания
    created_data = models.DateTimeField(auto_now_add=True)
    # Ссылка на курс
    course = models.ManyToManyField(Course, related_name="modules")

    def __str__(self):
        return self.name

'''
Модель задания.
Ссылается на задания
'''

class Task(models.Model):
    id = models.AutoField(primary_key=True)
    # Титул задания
    title = models.TextField()
    # Текст задания
    text = models.TextField()
    # Дата создания
    created_data = models.DateTimeField(auto_now_add=True)
    # Правильный ответ
    answer = models.IntegerField()
    # Ссылка на модуль
    module = models.ManyToManyField(Module, related_name="tasks")

    def __str__(self):
        return self.title