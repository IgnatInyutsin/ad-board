### content of "create_user.py" file
from django.contrib.auth import get_user_model
import os

# see ref. below
UserModel = get_user_model()

if not UserModel.objects.filter(username='root').exists():
    print("Creating")
    user=UserModel.objects.create_user('root', password=os.environ.get("ADMIN_PASSWORD", "root"))
    user.is_superuser=True
    user.is_staff=True
    user.save()